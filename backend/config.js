const GCP_KEY = null;
const AWS_KEY = null;
const AWS_SECRET = null;

export {
    GCP_KEY, AWS_KEY, AWS_SECRET
}
